# Overview

bytesize is a small library for converting a float to a human readable string
(ex. 32GB). It also includes the interface for marshalling/unmarshalling in
YAML.
