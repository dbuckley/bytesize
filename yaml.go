package bytesize

// MarshalYAML allows proper marshalling of bytsize strings like 32MB
func (b ByteSize) MarshalYAML() (interface{}, error) {
	return b.String(), nil
}

// UnmarshalYAML allows proper unmarshalling of bytsize strings like 32MB
func (b ByteSize) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var s string
	err := unmarshal(&s)
	if err != nil {
		return err
	}

	b, err = FromString(s)
	if err != nil {
		return err
	}

	return nil
}
