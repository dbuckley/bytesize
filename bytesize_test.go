package bytesize_test

import (
	"fmt"
	"testing"

	"gitlab.com/dbuckley/bytesize"
)

func Test_String(t *testing.T) {
	ts := []struct {
		name     string
		bytesize bytesize.ByteSize
		text     string
	}{
		{name: "KB", bytesize: 102400, text: "100.00KB"},
		{name: "MB", bytesize: 104857600, text: "100.00MB"},
		{name: "GB", bytesize: 107374182400, text: "100.00GB"},
		{name: "TB", bytesize: 109951162777600, text: "100.00TB"},
		{name: "PB", bytesize: 112589990684262400, text: "100.00PB"},
		{name: "EB", bytesize: 115292150460684697600, text: "100.00EB"},
		{name: "ZB", bytesize: 118059162071741130342400, text: "100.00ZB"},
		{name: "YB", bytesize: 120892581961462917470617600, text: "100.00YB"},
	}

	for _, ti := range ts {
		t.Run(ti.name, func(t *testing.T) {
			got := ti.bytesize.String()
			if got != ti.text {
				t.Error(fmt.Sprintf("Unexpected string of %s returned", got))
			}
		})
	}
}

func Test_FromString(t *testing.T) {
	ts := []struct {
		name     string
		bytesize bytesize.ByteSize
		text     string
	}{
		{name: "KB", bytesize: 102400, text: "100.00KB"},
		{name: "MB", bytesize: 104857600, text: "100.00MB"},
		{name: "GB", bytesize: 107374182400, text: "100.00GB"},
		{name: "TB, 3 char", bytesize: 1099511627776, text: "1TB"},
		{name: "TB", bytesize: 109951162777600, text: "100.00TB"},
		{name: "PB", bytesize: 112589990684262400, text: "100.00PB"},
		{name: "EB", bytesize: 115292150460684697600, text: "100.00EB"},
		{name: "ZB", bytesize: 118059162071741130342400, text: "100.00ZB"},
		{name: "YB", bytesize: 120892581961462917470617600, text: "100.00YB"},
	}

	for _, ti := range ts {
		t.Run(ti.name, func(t *testing.T) {
			got, err := bytesize.FromString(ti.text)
			if err != nil {
				t.Fatal(err)
			}
			gotI := int(*got)
			expect := int(ti.bytesize)
			if gotI != expect {
				t.Error(fmt.Sprintf("Unexpected bytesize of %d returned", gotI))
			}
		})
	}
}
