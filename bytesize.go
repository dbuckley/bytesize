package bytesize

import (
	"fmt"
	"strconv"
	"strings"
)

// ByteSize is a float representation of bytes (KB, MB, etc)
type ByteSize float64

const (
	_           = iota
	KB ByteSize = 1 << (10 * iota)
	MB
	GB
	TB
	PB
	EB
	ZB
	YB
)

// String is a method for printing bytsize as a formated string with prefix
func (b ByteSize) String() string {
	switch {
	case b >= YB:
		return fmt.Sprintf("%.2fYB", b/YB)
	case b >= ZB:
		return fmt.Sprintf("%.2fZB", b/ZB)
	case b >= EB:
		return fmt.Sprintf("%.2fEB", b/EB)
	case b >= PB:
		return fmt.Sprintf("%.2fPB", b/PB)
	case b >= TB:
		return fmt.Sprintf("%.2fTB", b/TB)
	case b >= GB:
		return fmt.Sprintf("%.2fGB", b/GB)
	case b >= MB:
		return fmt.Sprintf("%.2fMB", b/MB)
	case b >= KB:
		return fmt.Sprintf("%.2fKB", b/KB)
	}
	return fmt.Sprintf("%.2f", b)
}

// FromString parses a string to type ByteSize
func FromString(s string) (ByteSize, error) {
	var b ByteSize

	pfx := strings.ToUpper(s[len(s)-2:])
	switch pfx {
	case "YB":
		bs, err := strconv.ParseFloat(s[:len(s)-2], 64)
		if err != nil {
			return 0, err
		}
		b = ByteSize(bs) * YB
		return b, nil
	case "ZB":
		bs, err := strconv.ParseFloat(s[:len(s)-2], 64)
		if err != nil {
			return 0, err
		}
		b = ByteSize(bs) * ZB
		return b, nil
	case "EB":
		bs, err := strconv.ParseFloat(s[:len(s)-2], 64)
		if err != nil {
			return 0, err
		}
		b = ByteSize(bs) * EB
		return b, nil
	case "PB":
		bs, err := strconv.ParseFloat(s[:len(s)-2], 64)
		if err != nil {
			return 0, err
		}
		b = ByteSize(bs) * PB
		return b, nil
	case "TB":
		bs, err := strconv.ParseFloat(s[:len(s)-2], 64)
		if err != nil {
			return 0, err
		}
		b = ByteSize(bs) * TB
		return b, nil
	case "GB":
		bs, err := strconv.ParseFloat(s[:len(s)-2], 64)
		if err != nil {
			return 0, err
		}
		b = ByteSize(bs) * GB
		return b, nil
	case "MB":
		bs, err := strconv.ParseFloat(s[:len(s)-2], 64)
		if err != nil {
			return 0, err
		}
		b = ByteSize(bs) * MB
		return b, nil
	case "KB":
		bs, err := strconv.ParseFloat(s[:len(s)-2], 64)
		if err != nil {
			return 0, err
		}
		b = ByteSize(bs) * KB
		return b, nil
	}
	bs, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return 0, err
	}

	b = ByteSize(bs)
	return b, nil
}
